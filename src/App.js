import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./routes/Home";
import Detail from "./routes/Detail";
import Upcoming from "./routes/Upcoming";
import Popular from "./routes/Popular";
import TopRating from "./routes/TopRating";
import Header from "./components/Header";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/react-movie-task/upcoming" element={<Upcoming />} />
        <Route path="/react-movie-task/popular" element={<Popular />} />
        <Route path="/react-movie-task/top-rating" element={<TopRating />} />
        <Route path="/movie/:id" element={<Detail />} />
        <Route path="/react-movie-task" element={<Home />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
