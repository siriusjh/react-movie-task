import { useEffect, useState } from "react";
import MovieSlider from "../components/MovieSlider";
import { API_KEY, BASE_PATH, IMAGE_BASE_URL } from "../api";
import { currentday, nextyearday } from "../utils/Date";

function Upcoming() {
  //state
  const [loading, setLoading] = useState(true);
  const [movies, setMovies] = useState([]);

  // function
  const getMovies = async () => {
    const json = await (
      await fetch(
        `${BASE_PATH}/discover/movie?api_key=${API_KEY}&primary_release_date.gte=${currentday}&primary_release_date.lte=${nextyearday}`
      )
    ).json();
    const limited = json.results.filter((val, i) => i < 12);
    setMovies(limited);
    setLoading(false);
    console.log(json);
  };
  // useEffect
  useEffect(() => {
    getMovies();
  }, []);

  return (
    <>
      {loading ? (
        <h1>Loading..</h1>
      ) : (
        <div className="movies">
          <div className="movies-wrap">
            {movies.map((movie) => (
              <MovieSlider
                key={movie.id}
                id={movie.id}
                coverImg={`${IMAGE_BASE_URL}original${movie.poster_path}`}
                title={movie.title}
                date={movie.release_date}
              />
            ))}
          </div>
        </div>
      )}
    </>
  );
}
export default Upcoming;
