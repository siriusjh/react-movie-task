import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Wrap from "../components/Wrap";
import { API_KEY, BASE_PATH, IMAGE_BASE_URL } from "../api";
function Detail() {
  const { id } = useParams();
  const [detail, setDetail] = useState([]);
  const getMovie = async () => {
    const json = await (
      await fetch(`${BASE_PATH}/movie/${id}?api_key=${API_KEY}`)
    ).json();
    setDetail(json);
    console.log(json);
  };
  useEffect(() => {
    getMovie();
  }, []);
  return (
    <Wrap
      background={`${IMAGE_BASE_URL}original${detail.backdrop_path}`}
      image={`${IMAGE_BASE_URL}original${detail.poster_path}`}
      title={detail.title}
      intro={detail.overview}
      genre={"detail.genres"}
      runtime={detail.runtime}
      release_date={detail.release_date}
      vote_average={detail.vote_average}
    />
  );
}
export default Detail;
