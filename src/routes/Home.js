import { useEffect } from "react";
import { useState } from "react";
import Movie from "../components/Movie";
import styles from "./Home.module.css";
import { API_KEY, BASE_PATH, IMAGE_BASE_URL } from "../api";

function Home() {
  const [loading, setLoading] = useState(true);
  const [movies, setMovies] = useState([]);
  const getMovies = async () => {
    const json = await (
      await fetch(`${BASE_PATH}/movie/now_playing?api_key=${API_KEY}`)
    ).json();
    setMovies(json.results);
    setLoading(false);
  };
  useEffect(() => {
    getMovies();
  }, []);
  console.log(movies);
  return (
    <div className={styles.container}>
      {loading ? (
        <div className={styles.loader}>
          <span>Loading...</span>
        </div>
      ) : (
        <div className={styles.movies}>
          {movies.map((movie) => (
            <Movie
              key={movie.id}
              id={movie.id}
              date={movie.release_date}
              coverImg={`${IMAGE_BASE_URL}original${movie.poster_path}`}
              title={movie.title}
              summary={movie.overview}
              genres={movie.genre_ids}
              flag={"home"}
            />
          ))}
        </div>
      )}
    </div>
  );
}
export default Home;
